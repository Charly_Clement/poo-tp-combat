
<?php

    // JEUX DE COMBAT //

    class Personnage {

        private $_id,
                $_degats,
                $_nom;

        const CEST_MOI = 1; // Constante renvoyée par la méthode 'frapper' si on se frappe soi-même
        const PERSONNAGE_TUE = 2; // Constante renvoyée par la méthode 'frapper' si on a tué le personnage en le frappant
        const PERSONNAGE_FRAPPE = 3; // Constante renvoyée par la méthode 'frapper' si on a bien frappé le personnage

        public function __construct (array $donnees) {

            $this->hydrate($donnees);
        }

        public function frapper(Personnage $perso) {

            if ($perso->id() == $this->_id) {

                return self::CEST_MOI;

            }

            // On indique au personnage qu'il doit recevoir des dégats,
            // puis on retourne la valeur renvoyée par la méthode : self::PERSONNAGE_TUE ou SELF::PERSONNAGE_FRAPPE
            return $perso->recevoirDegats();
        }

        public function hydrate($donnees) {

            foreach ($donnees as $key => $value) {

                $method = 'set'.ucfirst($key);

                if (method_exists($this, $method)) {

                    $this->$method($value);
                }
            }
        }

        public function recevoirDegats() {

            $this->_degats += 5;

            // Si on a 100 de dégats ou plus, on dit que le personnage a été tué
            if ($this->_degats >= 100) {

                return self::PERSONNAGE_TUE;
            }

            // Sinon, on se contente de dire que le personnage a bien été frappé
            return self::PERSONNAGE_FRAPPE;
        }


    // GETTERS //

        public function degats() {

            return $this->_degats;
        }

        public function id() {

            return $this->_id;
        }

        public function nom() {

            return $this->_nom;
        }

        public function nomValide() {

            return!empty($this->_nom);
        }


    // SETTERS //

        public function setDegats($degats) {

            $degats = (int) $degats;

            if ($degats >= 0 && $degats <= 100) {

                $this->_degats = $degats;
            }
        }

        public function setId($id) {

            $id = (int) $id;

            if ($id > 0) {

                $this->_id = $id;
            }
        }

        public function setNom($nom) {

            if (is_string($nom)) {

                $this->_nom = $nom;
            }
        }

    }

    class PersonnagesManager {

        private $_db;

        public function __construct($db) {

            $this->setDb($db);
        }

        public function add(Personnage $perso) {

            $q = $this->_db->prepare('INSERT INTO personnages(nom) VALUES(:nom)');
            $q->bindValue(':nom', $perso->nom());
            $q->execute();

            $perso->hydrate([
                'id' => $this->_db->lastInsertId(),
                'degats' => 0,
            ]);
        }

        public function count() {

            return $this->_db->prepare('SELECT COUNT(*) FROM personnages')->fetchColumn();
        }

        public function delete(Personnage $perso) {

            $this->_db->exec('DELETE FROM personnages WHERE id = '.$perso->id());
        }

        public function exists($info) {

            if (is_int($info)) { // On veut voir si tel personne ayant pour id $info existe

                return (bool) $this->_db->query('SELECT COUNT(*) FROM personnages WHERE id = '.$info)->fetchColumn();
            }

            // Sinon, c'est qu'on veut vérifier si le nom existe ou pas
            $q = $this->_db->prepare('SELECT COUNT(*) FROM personnages WHERE nom = :nom');
            $q->execute([':nom' => $info]);

            return (bool) $q->fetchColumn();
        }

        public function get($info) {

            if (is_int($info)) {

                $q = $this->_db->query('SELECT id, nom, degats FROM personnages WHERE id = '.$info);
                $donnees = $q->fetch(PDO::FETCH_ASSOC);

                return new Personnage($donnees);
            }
            else {
                $q = $this->_db->prepare('SELECT id, nom, degats FROM personnages WHERE nom = :nom');
                $q->execute([':nom' => $info]);

                return new Personnage($q->fetch(PDO::FETCH_ASSOC));
            }
        }

        public function getList($nom) {

            $persos = [];

            $q = $this->_db->prepare('SELECT id, nom, degats FROM personnages WHERE nom <> :nom ORDER BY nom');
            $q->execute([':nom' => $nom]);

            while ($donnees = $q->fetch(PDO::FETCH_ASSOC)) {

                $persos[] = new Personnage($donnees);
            }
            
            return $persos;
        }

        public function update(Personnage $perso) {

            $q = $this->_db->prepare('UPDATE personnages SET degats = :degats WHERE id = :id');

            $q->bindValue(':degats', $perso->degats(), PDO::PARAM_INT);
            $q->bindValue('id', $perso->id(), PDO::PARAM_INT);

            $q->execute();
        }

        public function setDb(PDO $db) {

            $this->_db = $db;
        }

    }

//***********************************************************************************************************************************//

    // On enregistre notre autoload
    function chargerClasse($classname) {

        require $classname.'.php';
    }

    spl_autoload_register('chargerClasse');

    session_start(); // On appelle session_start() APRÈS avoir enregistré l'autoload

    if (isset($_GET['deconnexion'])) {

        session_destroy();
        header('Location: tp-combats.php');
        exit();
    }

    if (isset($_SESSION['perso'])) { // Si la session perso existe, on restaure l'objet

        $perso = $_SESSION['perso'];
    }


    $db = new PDO("mysql:host=localhost;dbname=combats", 'charly787', 'MaYa/2019');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);


    $manager = new PersonnagesManager($db);

    if (isset($_POST['creer']) && isset($_POST['nom'])) { // Si on a voulu créer un personnage

        $perso = new Personnage(['nom' => $_POST['nom']]); // On crée un nouveau personnage

        if (!$perso->nomValide()) {

            $message = 'Le nom choisi est invalide.';
            unset($perso);
        }
        elseif ($manager->exists($perso->nom())) {

            $message = 'Le nom du personnage est déjà pris.';
            unset($perso);
        }
        else {

            $manager->add($perso);
        }
    }
    elseif (isset($_POST['utiliser']) && isset($_POST['nom'])) { // Si on a voulu utiliser ce personnage

        if ($manager->exists($_POST['nom'])) { // Si celui-ci existe

            $perso = $manager->get($_POST['nom']);
        }
        else {

            $message = 'Ce personnage n\'existe pas !'; // S'il n'existe pas, on affiche ce message
        }
    }
    elseif (isset($_GET['frapper'])) { // Si on a cliqué sur un personnage pour le frapper
    
        if (!isset($perso)) {

            $message = 'Merci de créer un personnage ou de vous identifier.';
        }
        else {

            if (!$manager->exists((int) $_GET['frapper'])) {

                $message = 'Le personnage que vous voulez frapper n\'existe pas';
            }
            else {

                $persoAFrapper = $manager->get((int) $_GET['frapper']);

                $retour = $perso->frapper($persoAFrapper); // On stocke dans $retour les éventuelles erreurs ou messages
                                                           // que renvoie la méthode frapper
                                                           
                switch ($retour) {

                    case Personnage::CEST_MOI :
                        $message = 'Mais... pourquoi voulez vous vous frappé ???';

                        break;
                    
                    case Personnage::PERSONNAGE_FRAPPE :
                        $message = 'Le personnage a bien été frapper !';

                        $manager->update($perso);
                        $manager->update($persoAFrapper);

                        break;

                    case Personnage::PERSONNAGE_TUE :
                        $message = 'Vous avez tué ce personnage';

                        $manager->update($perso);
                        $manager->delete($persoAFrapper);
                    
                        break;

                }
            }
        }
    }


?>


<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Mon jeu de combats</title>
    </head>

    <body>

        <p>Nombre de personnages créés : <?= $manager->count() ?></p>

        <?php
        
            if (isset($message)) { // On a un message à afficher ?

                echo '<p>', $message, '</p>'; // Si oui, on l'affiche
            }

            if (isset($perso)) { // Si on utilise un personnage (nouveau ou pas)
        
        ?>

            <p><a href="?deconnexion=1">Déconnexion</a></p>

            <fieldset>
                <legend>Mes informations</legend>
                <p>
                    Nom : <?= htmlspecialchars($perso->nom()) ?><br>
                    Dégats : <?= $perso->degats() ?>
                </p>
            </fieldset>

            <fieldset>
                <legend>Qui frapper ?</legend>
                <p>
            <?php
            
                $persos = $manager->getList($perso->nom());

                if (empty($persos)) {

                    echo 'Personne à frapper !';
                }
                else {

                    foreach ($persos as $unPerso) {

                        echo '<a href="?frapper=', $unPerso->id(), '">', htmlspecialchars($unPerso->nom()),
                            '</a> (dégats : ', $unPerso->degats(), ')<br>';
                    }
                }
            
            ?>
                </p>
            </fieldset>

            <?php } else { ?>

            <form action="" method="post">

            <p>
                nom : <input type="text" name="nom" maxlength="50">
                <input type="submit" value="Créer ce personnage" name="creer">
                <input type="submit" value="Utiliser ce personnage" name="utiliser">
            </p>

            </form>

            <?php } ?>

    </body>
</html>
<?php

    if (isset($perso)) { // Si on a créé un personnage, on le stocke dans une variable session afin d'économiser une requête SQL

        $_SESSION['perso'] = $perso;
    }

?>


