<?php
    abstract class Personnage {

        protected   $atout,
                    $degats,
                    $id,
                    $nom,
                    $timeEndormi,
                    $type;

        const CEST_MOI = 1; // Constante renvoyée par la méthode `frapper` si on se frappe soit-même.
        const PERSONNAGE_TUE = 2; // Constante renvoyée par la méthode `frapper` si on a tué le personnage en le frappant.
        const PERSONNAGE_FRAPPE = 3; // Constante renvoyée par la méthode `frapper` si on a bien frappé le personnage.
        const PERSONNAGE_ENSORCELE = 4; // Constante renvoyée par la méthode `lancerUnSort` (voir classe Magicien) si on a bien ensorcelé un personnage.
        const PAS_DE_MAGIE = 5; // Constante renvoyée par la méthode `lancerUnSort` (voir classe Magicien) si on veut jeter un sort alors que la magie du magicien est à 0.
        const PERSO_ENDORMI = 6; // Constante renvoyée par la méthode `frapper` si le personnage qui veut frapper est endormi.
        
        public function __construct(array $donnees) {

            $this->hydrate($donnees);
            $this->type = strtolower(static::class);
        }

        public function estEndormi() {

            return $this->timeEndormi > time();
        }

        public function frapper(Personnage $perso) {

            if ($perso->id == $this->id) {

                return self::CEST_MOI;
            }

            if ($this->estEndormi()) {

                return self::PERSO_ENDORMI;
            }

            // On indique au personnage qu'il doit recevoir des dégâts.
            // Puis on retourne la valeur renvoyée par la méthode : self::PERSONNAGE_TUE ou self::PERSONNAGE_FRAPPE.
            return $perso->recevoirDegats();
        }

        public function hydrate(array $donnees) {

            foreach ($donnees as $key => $value) {

                $method = 'set'.ucfirst($key);

                if (method_exists($this, $method)) {

                    $this->$method($value);
                }
            }
        }

        public function nomValide() {

            return !empty($this->nom);
        }

        public function recevoirDegats() {

            $this->degats += 5;

            // Si on a 100 de dégâts ou plus, on supprime le personnage de la BDD.
            if ($this->degats >= 100) {

                return self::PERSONNAGE_TUE;
            }

            // Sinon, on se contente de mettre à jour les dégâts du personnage.
            return self::PERSONNAGE_FRAPPE;
        }

        public function reveil() {

            $secondes = $this->timeEndormi;
            $secondes -= time();
            
            $heures = floor($secondes / 3600);
            $secondes -= $heures * 3600;
            $minutes = floor($secondes / 60);
            $secondes -= $minutes * 60;
            
            $heures .= $heures <= 1 ? ' heure' : ' heures';
            $minutes .= $minutes <= 1 ? ' minute' : ' minutes';
            $secondes .= $secondes <= 1 ? ' seconde' : ' secondes';
            
            return $heures . ', ' . $minutes . ' et ' . $secondes;
        }

        public function atout() {

            return $this->atout;
        }

        public function degats() {

            return $this->degats;
        }

        public function id() {

            return $this->id;
        }

        public function nom() {

            return $this->nom;
        }

        public function timeEndormi() {

            return $this->timeEndormi;
        }

        public function type() {

            return $this->type;
        }

        public function setAtout($atout) {

            $atout = (int) $atout;

            if ($atout >= 0 && $atout <= 100) {

                $this->atout = $atout;
            }
        }

        public function setDegats($degats) {

            $degats = (int) $degats;

            if ($degats >= 0 && $degats <= 100) {

                $this->degats = $degats;
            }
        }

        public function setId($id) {

            $id = (int) $id;
            
            if ($id > 0) {

                $this->id = $id;
            }
        }

        public function setNom($nom) {

            if (is_string($nom)) {

                $this->nom = $nom;
            }
        }

        public function setTimeEndormi($time) {

            $this->timeEndormi = (int) $time;
        }
    }

    // GUERRIER //
    class Guerrier extends Personnage {

        public function recevoirDegats() {

            if ($this->degats >= 0 && $this->degats <= 25) {

                $this->atout = 4;
            }
            elseif ($this->degats > 25 && $this->degats <= 50) {

                $this->atout = 3;
            }
            elseif ($this->degats > 50 && $this->degats <= 75) {

                $this->atout = 2;
            }
            elseif ($this->degats > 75 && $this->degats <= 90) {

                $this->atout = 1;
            }
            else {

                $this->atout = 0;
            }

            $this->degats += 5 - $this->atout;

            // Si on a 100 de dégâts ou plus, on supprime le personnage de la BDD.
            if ($this->degats >= 100) {

                return self::PERSONNAGE_TUE;
            }

            // Sinon, on se contente de mettre à jour les dégâts du personnage.
            return self::PERSONNAGE_FRAPPE;
        }
    }


// MAGICIEN //
    class Magicien extends Personnage {

        public function lancerUnSort(Personnage $perso) {

            if ($this->degats >= 0 && $this->degats <= 25) {

                $this->atout = 4;
            }
            elseif ($this->degats > 25 && $this->degats <= 50) {

                $this->atout = 3;
            }
            elseif ($this->degats > 50 && $this->degats <= 75) {

                $this->atout = 2;
            }
            elseif ($this->degats > 75 && $this->degats <= 90) {

                $this->atout = 1;
            }
            else {

                $this->atout = 0;
            }

            if ($perso->id == $this->id) {

                return self::CEST_MOI;
            }

            if ($this->atout == 0) {

                return self::PAS_DE_MAGIE;
            }

            if ($this->estEndormi()) {

                return self::PERSO_ENDORMI;
            }

            $perso->timeEndormi = time() + ($this->atout * 6) * 3600;

            return self::PERSONNAGE_ENSORCELE;
        }
    }


// MANAGER //
    class PersonnagesManager {

        private $db; // Instance de PDO
        
        public function __construct($db) {

            $this->db = $db;
        }

        public function add(Personnage $perso) {

            $q = $this->db->prepare('INSERT INTO personnages_v2(nom, type) VALUES(:nom, :type)');

            $q->bindValue(':nom', $perso->nom());
            $q->bindValue(':type', $perso->type());

            $q->execute();

            $perso->hydrate([
                'id' => $this->db->lastInsertId(),
                'degats' => 0,
                'atout' => 0
            ]);
        }

        public function count() {

            return $this->db->query('SELECT COUNT(*) FROM personnages_v2')->fetchColumn();
        }

        public function delete(Personnage $perso) {

            $this->db->exec('DELETE FROM personnages_v2 WHERE id = '.$perso->id());
        }

        public function exists($info) {

            if (is_int($info)) { // On veut voir si tel personnage ayant pour id $info existe.

                return (bool) $this->db->query('SELECT COUNT(*) FROM personnages_v2 WHERE id = '.$info)->fetchColumn();
            }

            // Sinon, c'est qu'on veut vérifier que le nom existe ou pas.

            $q = $this->db->prepare('SELECT COUNT(*) FROM personnages_v2 WHERE nom = :nom');
            $q->execute([':nom' => $info]);

            return (bool) $q->fetchColumn();
        }

        public function get($info) {

            if (is_int($info)) {

                $q = $this->db->query('SELECT id, nom, degats, timeEndormi, type, atout FROM personnages_v2 WHERE id = '.$info);
                $perso = $q->fetch(PDO::FETCH_ASSOC);
            }
            else {

                $q = $this->db->prepare('SELECT id, nom, degats, timeEndormi, type, atout FROM personnages_v2 WHERE nom = :nom');
                $q->execute([':nom' => $info]);

                $perso = $q->fetch(PDO::FETCH_ASSOC);
            }

            switch ($perso['type']) {

            case 'guerrier': return new Guerrier($perso);
            case 'magicien': return new Magicien($perso);
            default: return null;
            }
        }
        
        public function getList($nom) {

            $persos = [];
            
            $q = $this->db->prepare('SELECT id, nom, degats, timeEndormi, type, atout FROM personnages_v2 WHERE nom <> :nom ORDER BY nom');
            $q->execute([':nom' => $nom]);
            
            while ($donnees = $q->fetch(PDO::FETCH_ASSOC)) {

                switch ($donnees['type']) {

                    case 'guerrier':
                        $persos[] = new Guerrier($donnees);
                    break;

                    case 'magicien':
                        $persos[] = new Magicien($donnees);
                    break;
                }
            }

            return $persos;
        }
        
        public function update(Personnage $perso) {

            $q = $this->db->prepare('UPDATE personnages_v2 SET degats = :degats, timeEndormi = :timeEndormi, atout = :atout WHERE id = :id');

            $q->bindValue(':degats', $perso->degats(), PDO::PARAM_INT);
            $q->bindValue(':timeEndormi', $perso->timeEndormi(), PDO::PARAM_INT);
            $q->bindValue(':atout', $perso->atout(), PDO::PARAM_INT);
            $q->bindValue(':id', $perso->id(), PDO::PARAM_INT);

            $q->execute();
        }
    }


// INDEX INDEX INDEX INDEX INDEX INDEX INDEX INDEX INDEX INDEX INDEX INDEX INDEX INDEX INDEX INDEX INDEX //
    function chargerClasse($classe) {

        require $classe . '.php';
    }

    spl_autoload_register('chargerClasse');

    session_start();

    if (isset($_GET['deconnexion'])) {

        session_destroy();
        header('Location: tp-perso-speciaux.php');
        exit();
    }

    $db = new PDO('mysql:host=localhost;dbname=combats_v2', 'charly787', 'MaYa/2019');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

    $manager = new PersonnagesManager($db);

    if (isset($_SESSION['perso'])) { // Si la session perso existe, on restaure l'objet.

        $perso = $_SESSION['perso'];
    }

    if (isset($_POST['creer']) && isset($_POST['nom'])) {  // Si on a voulu créer un personnage.

        switch ($_POST['type']) {

            case 'magicien' :
                $perso = new Magicien(['nom' => $_POST['nom']]);
            break;

            case 'guerrier' :
                $perso = new Guerrier(['nom' => $_POST['nom']]);
            break;

            default :
                $message = 'Le type du personnage est invalide.';
            break;
        }

        if (isset($perso)) { // Si le type du personnage est valide, on a créé un personnage.

            if (!$perso->nomValide()) {

                $message = 'Le nom choisi est invalide.';
                unset($perso);
            }
            elseif ($manager->exists($perso->nom())) {

            $message = 'Le nom du personnage est déjà pris.';
                unset($perso);
            }
            else {

                $manager->add($perso);
            }
        }
    }

    elseif (isset($_POST['utiliser']) && isset($_POST['nom'])) { // Si on a voulu utiliser un personnage.

        if ($manager->exists($_POST['nom'])) { // Si celui-ci existe.

            $perso = $manager->get($_POST['nom']);
        }
        else {

            $message = 'Ce personnage n\'existe pas !'; // S'il n'existe pas, on affichera ce message.
        }
    }

    elseif (isset($_GET['frapper'])) { // Si on a cliqué sur un personnage pour le frapper.

        if (!isset($perso)) {

            $message = 'Merci de créer un personnage ou de vous identifier.';
        }
        else {

            if (!$manager->exists((int) $_GET['frapper'])) {

                $message = 'Le personnage que vous voulez frapper n\'existe pas !';
            }
            else {

                $persoAFrapper = $manager->get((int) $_GET['frapper']);
                $retour = $perso->frapper($persoAFrapper); // On stocke dans $retour les éventuelles erreurs ou messages que renvoie la méthode frapper.

                switch ($retour) {

                    case Personnage::CEST_MOI :
                        $message = 'Mais... pourquoi voulez-vous vous frapper ???';
                    break;

                    case Personnage::PERSONNAGE_FRAPPE :
                        $message = 'Le personnage a bien été frappé !';

                        $manager->update($perso);
                        $manager->update($persoAFrapper);
                    break;

                    case Personnage::PERSONNAGE_TUE :
                        $message = 'Vous avez tué ce personnage !';

                        $manager->update($perso);
                        $manager->delete($persoAFrapper);
                    break;

                    case Personnage::PERSO_ENDORMI :
                        $message = 'Vous êtes endormi, vous ne pouvez pas frapper de personnage !';
                    break;
                }
            }
        }
    }

    elseif (isset($_GET['ensorceler'])) {

        if (!isset($perso)) {

            $message = 'Merci de créer un personnage ou de vous identifier.';
        }
        else {

            // Il faut bien vérifier que le personnage est un magicien.
            if ($perso->type() != 'magicien') {

            $message = 'Seuls les magiciens peuvent ensorceler des personnages !';
            }
            else {

                if (!$manager->exists((int) $_GET['ensorceler'])) {

                    $message = 'Le personnage que vous voulez frapper n\'existe pas !';
                }
                else {

                    $persoAEnsorceler = $manager->get((int) $_GET['ensorceler']);
                    $retour = $perso->lancerUnSort($persoAEnsorceler);

                    switch ($retour) {

                    case Personnage::CEST_MOI :
                        $message = 'Mais... pourquoi voulez-vous vous ensorceler ???';
                    break;

                    case Personnage::PERSONNAGE_ENSORCELE :
                        $message = 'Le personnage a bien été ensorcelé !';

                        $manager->update($perso);
                        $manager->update($persoAEnsorceler);
                    break;

                    case Personnage::PAS_DE_MAGIE :
                        $message = 'Vous n\'avez pas de magie !';
                    break;
                    
                    case Personnage::PERSO_ENDORMI :
                        $message = 'Vous êtes endormi, vous ne pouvez pas lancer de sort !';
                    break;
                    }
                }
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="fr">

    <head>
        <title>TP : Mini jeu de combat - Version 2</title>

        <meta charset="utf-8" />
    </head>

    <body>

        <p>Nombre de personnages créés : <?= $manager->count() ?></p>

    <?php
        if (isset($message)) { // On a un message à afficher ?

            echo '<p>', $message, '</p>'; // Si oui, on l'affiche
        }

        if (isset($perso)) { // Si on utilise un personnage (nouveau ou pas).

    ?>
        <p><a href="?deconnexion=1">Déconnexion</a></p>

        <fieldset>

            <legend>Mes informations</legend>
        <p>
            Type : <?= ucfirst($perso->type()) ?><br />
            Nom : <?= htmlspecialchars($perso->nom()) ?><br />
            Dégâts : <?= $perso->degats() ?><br />
    <?php
    // On affiche l'atout du personnage suivant son type.
        switch ($perso->type()) {

            case 'magicien' :
                echo 'Magie : ';
            break;

            case 'guerrier' :
                echo 'Protection : ';
            break;
        }

        echo $perso->atout();
    ?>
        </p>
        </fieldset>

        <fieldset>
        <legend>Qui attaquer ?</legend>
        <p>
    <?php
        // On récupère tous les personnages par ordre alphabétique, dont le nom est différent de celui de notre personnage (on va pas se frapper nous-même :p).
        $retourPersos = $manager->getList($perso->nom());

        if (empty($retourPersos)) {

            echo 'Personne à frapper !';
        }
        else {

            if ($perso->estEndormi()) {

                echo 'Un magicien vous a endormi ! Vous allez vous réveiller dans ', $perso->reveil(), '.';
            }

            else {

                foreach ($retourPersos as $unPerso) {

                echo '<a href="?frapper=', $unPerso->id(), '">', htmlspecialchars($unPerso->nom()), '</a> (dégâts : ', $unPerso->degats(), ' | type : ', $unPerso->type(), ')';
                
                // On ajoute un lien pour lancer un sort si le personnage est un magicien.
                if ($perso->type() == 'magicien') {

                    echo ' | <a href="?ensorceler=', $unPerso->id(), '">Lancer un sort</a>';
                }

                echo '<br />';
                }
            }
        }
    ?>
        </p>
        </fieldset>

    <?php } else { ?>

        <form action="" method="post">
            <p>
                Nom : <input type="text" name="nom" maxlength="50" /> <input type="submit" value="Utiliser ce personnage" name="utiliser" /><br />
                Type :
                <select name="type">
                <option value="magicien">Magicien</option>
                <option value="guerrier">Guerrier</option>
                </select>
                <input type="submit" value="Créer ce personnage" name="creer" />
            </p>
        </form>

    <?php } ?>

    </body>
    </html>

    <?php
        if (isset($perso)) { // Si on a créé un personnage, on le stocke dans une variable session afin d'économiser une requête SQL.

            $_SESSION['perso'] = $perso;
        }
    ?>
