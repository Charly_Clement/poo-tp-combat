
<?php

    class Personnage {

        private $_force;
        private $_localisation; // <-- FONCÉ, attribut déclaré mais non utilisée
        private $_experience;
        private $_degats;
        // Erreur lors de l'appel d'un attribut en dehors de la classe

        // Le constructeur, exécuté dès la crétion de l'objet, initialiser des attributs ou faire une connexion à la BDD
        public function __construct($force, $degats) { // Constructeur demandant 2 paramètres

            echo 'Voici le constructeur'; // Message s'affichant une fois que tout objet est crée
            $this->setForce($force);   // Initialisation de la force
            $this->setDegats($degats); // Initialisation des dégats
            $this->_experience = 1;    // Initialisation de l'expérience à 1
        }

        public function frapper(Personnage $persoAFrapper) {

            $persoAFrapper->_degats += $this->_force;
        }

        public function gagnerExperience() {

            $this->_experience ++;
        }

        // Accesseurs OU getters, portent le même nom que l'attribut

        // Méthode degats() qui se charge de renvoyer le contenu de l'attribut $_degats
        public function degats() {

            return $this->_degats;
        }
        
        public function force() {

            return $this->_force;
        }

        public function experience() {

            return $this->_experience;
        }

        // Mutateur OU setters

        // Mutateur chargé de modifier l'attribut $_experience
        public function setExperience($experience) {

            if (!is_int($experience)) { // S'il ne s'agit pas d'un nombre entier

                // trigger error = déclenche une erreur ultilisateur
                trigger_error('L\'experience d\'un personnage doit être un nombre entier',E_USER_WARNING);
                return;
            }

            if ($experience > 100) { // On vérifie bien qu'on ne souhaite pas assigner une valeur supérieure à 100

                trigger_error('L\'experience d\'un personnage ne peut dépasser 100',E_USER_WARNING);
                return;
            }
            $this->_experience = $experience;

        }

        // Mutateur chargé de modifier l'attribut $_force
        public function setforce($force) {

            if (!is_int($force)) {

                trigger_error('La force d\'un personnage doit être un nombre entier',E_USER_WARNING);
                return;
            }

            if ($force > 100) {

                trigger_error('La force d\'un personnage ne peut dépasser 100',E_USER_WARNING);
                return;
            }
            $this->_force = $force;
            
        }

    }

    // new = création d'un nouvel objet. On instancie la classe Personnage
    $perso1 = new Personnage(60, 0); // 60 de force, 0 dégats. Paramètre demandé par le constructeur

    // création d'un autre objet
    $perso2 = new Personnage(100, 10); // 100 de force, 10 dégats. Paramètre demandé par le constructeur

    // On peut modifier, juste après la création des personnages, leur valeurs et leur forces
    $perso1->setForce(10);
    $perso1->setExperience(2);

    $perso2->setForce(90);
    $perso2->setExperience(58);
    
    $perso1->frapper($perso2);
    $perso1->gagnerExperience();

    $perso2->frapper($perso1);
    $perso2->gagnerExperience();
    
    echo 'Le personnage 1 a ', $perso1->force(),' de force,
        contrairement au personnage 2 qui a ', $perso2->force(), ' de force.<br>';

    echo 'Le personnage 1 a ', $perso1->experience(), ' d\'experience,
        contrairement au personnage 2 qui a ' , $perso2->experience(), ' d\'expérience.<br>';

    echo 'Le personnage 1 a ', $perso1->degats(), ' de dégats,
        contrairement au personnage 2 qui a ', $perso2->degats(), ' de dégats.<br>';


    // AUTO-CHARGEMENT DES CLASSES //

    require 'MaClasse.php'; // Inclure la classe

    $objet = new MaClasse; // Seulement après se servir de la classe

    function chargerClasse($classe) { // Fonction pour charger des classes

        require $classe . '.php';
    }
    spl_autoload_register('chargerClasse'); // On enregistre la fonction en autoload pour qu'elle soit appelée
                                            //  dès qu'on instenciera une classe non déclarée
    $perso = new Personnage(50, 20);



    // L'OPÉRATEUR DE RÉSOLUTION DE PORTÉE ( :: ) Scope Resolution Operator //

    class Personne {

        private $_force;
        private $_localisation;
        private $_experience;
        private $_degats;

    // Déclarations des constantes de classe (Paamayim Nekudotayim) en rapport avec la force.
    // Pas de $ devant et toujours en MAJUSCULES
        const FORCE_PETITE = 20;
        const FORCE_MOYENNE = 50;
        const FORCE_GRANDE = 80;

        // Variable statique PRIVÉE
        private static $_texteADire = 'Je vous vaincrais tous !!';
        // On peut aussi mettre le static devant le type
        // static private $_texteADire = 'Je vous vaincrais tous !!';


        public function __construct($forceInitiale) {

            // N'oubliez pas qu'il faut assigner la valeur d'un attribut uniquement depuis son setter !
            $this->setForce($forceInitiale);
        }

        public function setForce($force) {

        // On vérifie qu'on nous donne bien soit une « FORCE_PETITE », soit une « FORCE_MOYENNE », soit une « FORCE_GRANDE ».
            if (in_array($force, [self::FORCE_PETITE, self::FORCE_MOYENNE, self::FORCE_GRANDE])) {

                $this->_force = $force;
            }
        }

        // MÉTHODE STATIQUE :
        // Le mot clé static peut être placé avant la visibilité de la méthode
        public static function parler() {

            echo 'Je gagnerais ce combat !!';
            Personne::parler();

            // ATTRIBUT STATIQUE :
            echo self::$_texteADire; // On donne le texte à dire
        }

    }
    
    // On envoie une force << FORCE MOYENNE >> en guise de force initiale
    $perso = new Personne(Personne::FORCE_MOYENNE);

    $perso3 = new Personne(Personne::FORCE_GRANDE);
    $perso3->parler();



    class Compteur {

        private static $_compteur = 0;

        public function __construct() {

            // On instancie la variable $_compteur qui appartient à la classe (donc utilisation du mot-clé self)
            self::$_compteur++;

        }

        public static function getCompteur() { // Méthode statique qui renverra la valeur du compteur

            return self::$_compteur;
        }

    }

    $test1 = new Compteur;
    $test2 = new Compteur;
    $test3 = new Compteur;

    echo Compteur::getCompteur();


?>
