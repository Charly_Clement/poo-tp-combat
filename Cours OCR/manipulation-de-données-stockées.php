
<?php

class Personnage {

        private $_id;
        private $_nom;
        private $_forcePerso;
        private $_degats;
        private $_niveau;
        private $_experience;

    // Liste des getters

    public function id()         { return $this->_id; }

    public function nom()        { return $this->_nom; }

    public function forcePerso() { return $this->_forcePerso; }

    public function degats()     { return $this->_degats; }

    public function niveau()     { return $this->_niveau; }

    public function experience() { return $this->_experience; }


    // Liste des setters

    public function setId($id) {

        // L'identifiant du personnage sera, quoi qu'il arrive, un nombre entier
            $this->_id = (int) $id;
        }

    public function setNom($nom) {

        // On vérifie qu'il s'agit bien d'une chaîne de caractères
        // dont la longueur est inférieure à 30 caractères
        if (is_string($nom) && strlen($nom) <=30) {

            $this->_nom = $nom;
        }
    }

    public function setForcePerso($forcePerso) {

        $forcePerso = (int) $forcePerso;
        
        // On vérifie que la force passée est comprise entre 0 et 100
        if ($forcePerso >= 1 && $forcePerso <= 100) {

            $this->_forcePerso = $forcePerso;
        }
    }

    public function setDegats($degats) {

        $degats = (int) $degats;
        
        // On vérifie que les dégats passée sont compris entre 0 et 100
        if ($degats >= 0 && $degats <= 100) {

            $this->_degats = $degats;
        }
    }

    public function setNiveau($niveau) {

        $niveau = (int) $niveau;
        
        // On vérifie que le niveau n'est pas négatif
        if ($niveau >= 0) {

            $this->_niveau = $niveau;
        }
    }

    public function setExperience($experience) {

        $experience = (int) $experience;
        
        if ($experience >= 0 && $experience <= 100) {
            
            $this->_experience = $experience;
        }
    }
}

    $donnees = [
        'id' => 16,
        'nom' => 'Vyk12',
        'forcePerso' => 5,
        'degats' => 55,
        'niveau' => 4,
        'experience' => 20
        ];

class A {

    // FONCTION HYDRATE //

        // Un tableau de données doit être passé à la fonction (d'où le préfixe "array")
        public function hydrate(array $donnees) {

            foreach ($donnees as $key => $value) {
                
                // ucfirst sert à mettre la première lettre en majuscules, correspondant au setter : setNom, setdegats...
                $method = 'set' . ucfirst($key);

                // Si le setter correspondant existe
                if (method_exists($this, $method)) {

                    // On appelle le setter
                    $this->$method($value);
                }
            }

        }
}

class PersonnagesManager {

    private $_db; // Instance de PDO.

    public function __construct($db) {

        $this->setDb($db);
    }

    public function add(Personnage $perso) {

        // Préparation de la requête d'insertion.
        $q = $this->_db->prepare('INSERT INTO personnages(nom, forcePerso, degats, niveau, experience)
                                    VALUES(:nom, :forcePerso, :degats, :niveau, :experience)');
        // Exécution de la requête.
        $q->bindValue(':nom', $perso->nom());
        $q->bindValue(':forcePerso', $perso->forcePerso(), PDO::PARAM_INT);
        $q->bindValue(':degats', $perso->degats(), PDO::PARAM_INT);
        $q->bindValue(':niveau', $perso->niveau(), PDO::PARAM_INT);
        $q->bindValue(':experience', $perso->experience(), PDO::PARAM_INT);

        $q->execute();

    }

    public function delete(Personnage $perso) {

        // Exécute une requête de type DELETE.
        $this->_db->exec('DELETE FROM Personnages WHERE id = '.$perso->id());
    }

    public function get($id) {

        $id = (int) $id;

        // Exécute une requête de type SELECT avec une clause WHERE, et retourne un objet Personnage.
        $q = $this->_db->query('SELECT id, nom, forcePerso, degats, niveau, experience FROM personnages WHERE id = '.$id);
        
        $donnees = $q->fetch(PDO::FETCH_ASSOC);

        return NEW Personnage($donnees);
    }

    public function getList() {

        // Retourne la liste de tous les personnages.
        $persos = [];

        $q = $this->_db->query('SELECT id, nom, forcePerso, degats, niveau, experience FROM personnages ORDER BY nom');

        while ($donnees = $q->fetch(PDO::FETCH_ASSOC)) {

            $persos[] = new Personnage($donnees);
        }

        return $persos;
    }

    public function update(Personnage $perso) {

        // Prépare une requête de type UPDATE.
        $q = $this->_db->prepare('UPDATE personnages SET forcePerso = :forcePerso, degats = :degats,
                                            niveau = :niveau, experience = :experience');
        
        $q->bindValue(':forcePerso', $perso->forcePerso(), PDO::PARAM_INT);
        $q->bindValue(':degats', $perso->degats(), PDO::PARAM_INT);
        $q->bindValue(':niveau', $perso->niveau(), PDO::PARAM_INT);
        $q->bindValue(':experience', $perso->experience(), PDO::PARAM_INT);
        $q->bindValue(':id', $perso->id(), PDO::PARAM_INT);

        $q->execute();

    }

    public function setDb(PDO $db) {

        $this->_db = $db;
    }
}


    $perso = new Personnage([

        'nom' => 'Charly',
        'forcePerso' => 5,
        'degats' => 0,
        'niveau' => 1,
        'experience' => 0
    ]);

    $db = new PDO('mysql:host=localhost;dbname=combats', 'charly787', 'MaYa/2019');
    $manager = new PersonnagesManager($db);

    $manager->add($perso);

?>