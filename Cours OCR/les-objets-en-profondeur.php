
<?php

    //// UN OBJET, UN IDENTIFIANT ////

    /* $objet = new MaClasse; , la variable $objet contient l'objet que l'on vient de créer.
       $objet ne contient pas l'objet lui-même, mais son identifiant unique. */

    class MaClasseUne {

        public $attribut1;
        public $attribut2;
    }

    $a = new MaClasseUne;

    $b = $a; // On assigne à $b l'identifiant de $a, donc $a et $b représentent le même objet (et donc le même identifiant).

    $a->attribut1 = 'Hello';
    echo $b->attribut1; // Affiche Hello.

    $b->attribut2 = 'Salut';
    echo $a->attribut2; // Affiche Salut.

    /* RÉFÉRENCE : Une référence n'est rien d'autre qu'une variable pointant sur une autre : elle devient un alias de la variable
       sur laquelle elle pointe. Si vous modifiez l'une de ces variables, les deux prendront la même valeur.
       Lors de la déclaration d'une référence, on fait précéder le nom de la variable à référencer d'un & ;
       et ce uniquement lors de sa déclaration. */



    // CLÔNER UN OBJET //

    $objet2 = clone $objet1; /* Les deux objets ont maintenant un identifiant différent,
                                on peut donc modifier un de ces objets sans modifier l'autre. */

    /* Lorsque vous clonez un objet, la méthode __clone du nouvel objet sera appelée (du moins, si vous l'avez définie).
       Vous ne pouvez pas appeler cette méthode directement. C'est la méthode __clone du nouvel objet créé qui est appelée,
       pas la méthode __clone de l'objet à cloner. */

    class MaClasseDeux {

        private static $instances = 0;


        public function __construct() {

            self::$instances++;
        }

        public function __clone() {

            self::$instances++;
        }

        public static function getInstances() {

            return self::$instances;
        }
    }

    $a = new MaClasseDeux;
    $b = clone $a;

    echo 'Nombre d\'instances de MaClasse : ', MaClasseDeux::getInstances();

    // Résultat : Nombre d'instances de MaClasseDeux: 2



    //// COMPARER DES OBJETS ////

    /* Avec l'opérateur == :
       qui vérifie que les deux objets sont issus de la même classe et que les valeurs de chaque attributs sont identiques. */
    if ($objet1 == $objet2) {

      echo '$objet1 et $objet2 sont identiques !';
    }
    else {

      echo '$objet1 et $objet2 sont différents !'; // <-- également si les objets ne sont pas de la même instance de classe.
    }

    /* Résultat : $a != $b
                  $a == $c */


    /* Avec l'opérateur === :
       qui vérifie que les deux identifiants d'objet sont les mêmes. */
    class A {

        public $attribut1;
        public $attribut2;
    }

    $a = new A;
    $a->attribut1 = 'Hello';
    $a->attribut2 = 'Salut';

    $b = new A;
    $b->attribut1 = 'Hello';
    $b->attribut2 = 'Salut';

    $c = $a;

    if ($a === $b) {

        echo '$a === $b';
    }
    else {

        echo '$a !== $b';
    }

    echo '<br />';


    if ($a === $c) {

        echo '$a === $c';
    }
    else {

        echo '$a !== $c';
    }

    /* Résultat : $a !== $b
                  $a === $c */



    //// PARCOURIR LES OBJETS ////

    // Le fait de parcourir un objet consiste à lire tous les attributs visibles de l'objet.

    /* Deux possibilités de lire un tableau : */

    class MaClasse {

        public $attribut1 = 'Premier attribut public';
        public $attribut2 = 'Deuxième attribut public';

        protected $attributProtege1 = 'Premier attribut protégé';
        protected $attributProtege2 = 'Deuxième attribut protégé';

        private $attributPrive1 = 'Premier attribut privé';
        private $attributPrive2 = 'Deuxième attribut privé';

        function listeAttributs() {

            foreach ($this as $attribut => $valeur) {

                echo '<strong>', $attribut, '</strong> => ', $valeur, '<br />';
            }
        }
    }

    class Enfant extends MaClasse {

        function listeAttributs() { // Redéclaration de la fonction pour que ce ne soit pas celle de la classe mère qui soit appelée.

            foreach ($this as $attribut => $valeur) {

                echo '<strong>', $attribut, '</strong> => ', $valeur, '<br />';
            }
        }
    }

    $classe = new MaClasse;
    $enfant = new Enfant;

    echo '---- Liste les attributs depuis l\'intérieur de la classe principale ----<br />';
    $classe->listeAttributs();

    echo '<br />---- Liste les attributs depuis l\'intérieur de la classe enfant ----<br />';
    $enfant->listeAttributs();

    echo '<br />---- Liste les attributs depuis le script global ----<br />';

    foreach ($classe as $attribut => $valeur) {

        echo '<strong>', $attribut, '</strong> => ', $valeur, '<br />';
    }

    /**
     *  Résultat :

     * ---- Liste des attributs depuis l'intérieur de la classe principale ----
     *          attribut1 => Premier attribut public
     *          attribut2 => Deuxième attribut public
     *          attributProtege1 => Premier attribut protégé
     *          attributProtege2 => Deuxième attribut protégé
     *          attributPrive1 => Premier attribut privé
     *          attributPrive2 => Deuxième attribut privé

     * ---- Liste des attributs depuis l'intérieur de la classe enfant ----
     *          attribut1 => Premier attribut public
     *          attribut2 => Deuxième attribut public
     *          attributProtege1 => Premier attribut protégé
     *          attributProtege2 => Deuxième attribut protégé

     * ---- Liste des attributs depuis le script global ----
     *          attribut1 => Premier attribut public
     *          attribut2 => Deuxième attribut public

     */

?>
