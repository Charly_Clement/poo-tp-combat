
<?php

    class Personnage { // Classe Personnage de base

    }

    class Magicien extends Personnage { // Classe Magicien héritant de Personnage

        private $_magie; // Ajout d'un attribut, en plus de ceux dont il a hérité.
                         // Indique la puissance du magicien sur 100, sa capacité à produire de la magie
        protected $protege; // SANS UNDERSCORE, Type modifié du type private, exectement les même effets,
                             // sauf que toute classe filles aura accès aux éléments protégés
        // De manière générale, toujours utilisé le type protected

        public function lancerUnSort($perso) {

            $perso->recevoirDegats($this->magie); // On va dire que la magie du magicien représente sa force
        }

        public function gagnerExperience() {

            // On appelle la méthode gagnerExperience de la classe parente
            parent::gagnerExperience();

            if ($this->_magie < 100) {

                $this->_magie += 10;
            }
        }
        
        // Si la méthode parente retourne une valeur, vous pouvez la récupérer comme si vous appeliez une méthode normalement
        public function niveau() {

            $retour = parent::niveau();

            echo $retour;
        }
        // $b = New Magicien;
        // $b->niveau();

        public function MagicienBlanc() { // Classe MagicienBlanc héritant de tous les attributs et méthodes de Magicien et de Personnage

        }
    }


    // IMPOSER DES CONTRAINTES : ABSTRACTION ET FINALISATION //

    // Classe abstraite
    abstract class Combattant { // La classe Combattant est abstraite
                                // Garantit qu'aucun objets Combattant ne sera créé par étourderie

    // Méthode abstraite

        // On va forcer toute classe fille à écrire cette méthode car chaque combattant frappe différemment
        abstract public function taper(Combattant $perso); 

        // Cette méthode n'aura pas besoin d'être réécrite
        public function courir() {

        }
        // !! Pour définir une méthode comme étant abstraite, il faut que la classe elle-même soit abstraite !!
    }

    // Classe finale

    // Class abstraite servant de modèle
    abstract class Maison {

    }

    // Classe finale, on ne pourra pas créer de classe héritant de Piece
    final class Piece extends Maison {

    }

    // class Salon extends Piece {
        
    // }
    // Erreur fatale, car la classe hérite d'une classe finale


    // Méthode finale
    // Même résultat que pour la classe, si la méthode parente est en finale, celà créera une erreur 


    // Résolution statique à la volée //

    // static appelle l'élément de la classe qui est appelée pendant l'exécution
    class Mere {
        public static function lancerLeTest() {
            self::quiEstCe();
        }
        
        public static function quiEstCe() {
            echo 'Je suis la classe Mere !';
        }
        }

    class Enfant extends Mere {
        public static function quiEstCe() {
            echo 'Je suis la classe Enfant !';
        }
    }

    Enfant::lancerLeTest();
    // Résultat affiché : 'Je suis la classe Mere'

?>