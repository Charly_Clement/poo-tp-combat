
<?php

    // Lancer une exception //

    function premiereAddition($a, $b) {

        if (!is_numeric($a) || !is_numeric($b)) {

            // On lance une nouvelle exception grâce à throw (lancer) et on instancie directement un objet de la classe Exception.
            throw new Exception('Les deux paramètres doivent être des nombres');
        }

    return $a + $b;
    }

    // Attraper un exception //

    try {

        // Sans try catch, celà affiche une erreur fatal.
        echo premiereAddition(12, 3), '<br />';
        echo premiereAddition('azerty', 54), '<br />';
        echo premiereAddition(4, 8);
    }
    catch (Exception $e) { // On va attraper les exceptionx "Exception" s'il y en a une qui est levée.

        echo 'Une exception a été lancée. Message d\'erreur : '. $e->getMessage();
    }
    // Résultat : Une exception a été lancée. Message d'erreur : Les deux paramètres doivent être des nombres.

    /* La troisième instruction n'a pas été exécutée puisque la deuxième instruction interrompt la lecture du bloc.
       Si vous interceptez les exceptions comme ceci, alors le script n'est pas interrompu. */

    // Ne jamais lancer d'exception dans un destructeur.



    //// HÉRITER LA CLASSE EXCEPTION ////

    /* Liste des attributs et méthodes de la classe Exception :

        protected $message = 'Exception inconnue'; // message de l'exception
        private   $string;                         // __toString cache
        protected $code = 0;                       // code de l'exception défini par l'utilisateur
        protected $file;                           // nom du fichier source de l'exception
        protected $line;                           // ligne de la source de l'exception
        private   $trace;                          // backtrace
        private   $previous;                       // exception précédente (depuis PHP 5.3)

        public function __construct($message = null, $code = 0, Exception $previous = null);

        final private function __clone();         // Inhibe le clonage des exceptions.

        final public function getMessage();       // message de l'exception
        final public function getCode();          // code de l'exception
        final public function getFile();          // nom du fichier source
        final public function getLine();          // ligne du fichier source
        final public function getTrace();         // un tableau de backtrace()
        final public function getPrevious();      // exception précédente (depuis PHP 5.3)
        final public function getTraceAsString(); // chaîne formatée de trace

        // Remplacable
        public function __toString();                    // chaîne formatée pour l'affichage

    */

    class Exception {

        protected $message = 'exception inconnu'; // Message de l'exception.
        protected $code = 0; // Code de l'exception défini par l'utilisateur.
        protected $file; // Nom du fichier source de l'exception.
        protected $line; // Ligne de la source de l'exception.

        final function getMessage(); // Message de l'exception.
        final function getCode(); // Code de l'exception.
        final function getFile(); // Nom du fichier source.
        final function getLine(); // Ligne du fichier source.
        final function getTrace(); // Un tableau de backtrace().
        final function getTraceAsString(); // Chaîne formattée de trace.

        /* Remplacable */
        function __construct ($message = NULL, $code = 0);
        function __toString(); // Chaîne formatée pour l'affichage.
    }


    class MonExceptionUne extends Exception {

        public function __construct($message, $code = 0) {

            parent::__construct($message, $code);
        }

        public function __toString() {

            return $this->message;
        }
    }

    function deuxiemeAddition($a, $b) {

        if (!is_numeric($a) || !is_numeric($b)) {

            throw new MonExceptionUne('Les deux paramètres doivent être des nombres'); // On lance une exception "MonException".
        }

        return $a + $b;
    }

    try { // Nous allons essayer d'effectuer les instructions situées dans ce bloc.

        echo deuxiemeAddition(12, 3), '<br />';
        echo deuxiemeAddition('azerty', 54), '<br />';
        echo deuxiemeAddition(4, 8);
    }
    catch (MonExceptionUne $e) { // Nous allons attraper les exceptions "MonException" s'il y en a une qui est levée.

        echo $e; // On affiche le message d'erreur grâce à la méthode __toString que l'on a écrite.
    }

    echo '<br />Fin du script'; // Ce message s'affiche, ça prouve bien que le script est exécuté jusqu'au bout.

    /* Nous avons attrapé uniquement les exceptions de type MonException. Si lancez une exception Exception à la place,
       elle ne sera pas attrapée. Si vous décidez d'attraper, dans le bloc catch, les exceptions Exception, alors toutes les
       exceptions seront attrapées car elles héritent toutes de cette classe. En fait, quand vous héritez une classe d'une autre
       et que vous décidez d'attraper les exceptions de la classe parente, alors celles de la classe enfant le seront aussi. */



    //// Emboîter plusieurs bloc catch ////

    /* Il est possible d'emboîter plusieurs blocs catch. Vous pouvez mettre un premier bloc attrapant les exceptions MonException
       suivi d'un deuxième attrapant les exceptions Exception. Si vous effectuez une telle opération et qu'une exception est lancée,
       alors PHP ira dans le premier bloc pour voir si ce type d'exception doit être attrapé, si tel n'est pas le cas il va dans
       le deuxième, etc., jusqu'à ce qu'il tombe sur un bloc qui l'attrape. Si aucun ne l'attrape, alors une erreur fatale est levée. */

    class MonExceptionDeux extends Exception {

        public function __construct($message, $code = 0) {

            parent::__construct($message, $code);
        }

        public function __toString() {

            return $this->message;
        }
    }

    function troisièmeAddition($a, $b) {

        if (!is_numeric($a) || !is_numeric($b)) {

            throw new MonExceptionDeux('Les deux paramètres doivent être des nombres'); // On lance une exception "MonException".
        }

        if (func_num_args() > 2) {

            throw new Exception('Pas plus de deux arguments ne doivent être passés à la fonction');
            // Cette fois-ci, on lance une exception "Exception".
        }

        return $a + $b;
    }

    try { // Nous allons essayer d'effectuer les instructions situées dans ce bloc.

        echo troisièmeAddition(12, 3), '<br />';
        echo troisièmeAddition(15, 54, 45), '<br />';
    }

    catch (MonExceptionDeux $e) { // Nous allons attraper les exceptions "MonException" s'il y en a une qui est levée.

        echo '[MonException] : ', $e; // On affiche le message d'erreur grâce à la méthode __toString que l'on a écrite.
    }

    catch (Exception $e) { // Si l'exception n'est toujours pas attrapée, alors nous allons essayer d'attraper l'exception "Exception".

        echo '[Exception] : ', $e->getMessage();
        // La méthode __toString() nous affiche trop d'informations, nous voulons juste le message d'erreur.
    }

    echo '<br />Fin du script'; // Ce message s'affiche, cela prouve bien que le script est exécuté jusqu'au bout.

    /* Cette fois-ci, aucune exception MonException n'est lancée, mais une exception Exception l'a été.
       PHP va donc effectuer les opérations demandées dans le deuxième bloccatch. */



    //// La classe PDOException ////

    /* 
        Liste des exceptions PDO :       |     Arbre de la classe SPL Exceptions ¶

        BadFunctionCallException         |   LogicException (étend la classe Exception)
        BadMethodCallException           |      BadFunctionCallException
        DomainException                  |          BadMethodCallException
        InvalidArgumentException         |
        LengthException                  |      DomainException
        LogicException                   |      InvalidArgumentException
        OutOfBoundsException             |      LengthException
        OutOfRangeException              |      OutOfRangeException
        OverflowException                |   
        RangeException                   |   RuntimeException (étend la classe Exception)
        RuntimeException                 |   
        UnderflowException               |      OutOfBoundsException
        UnexpectedValueException         |      OverflowException
                                         |      RangeException
                                         |      UnderflowException
                                         |      UnexpectedValueException
    */

    // Exemple d'utilisation de PDOException :

    try {

        $db = new PDO('mysql:host=localhost;dbname=tests', 'root', ''); // Tentative de connexion.
        echo 'Connexion réussie !'; // Si la connexion a réussi, alors cette instruction sera exécutée.
    }
    catch (PDOException $e) { // On attrape les exceptions PDOException.

        echo 'La connexion a échoué.<br />';
        echo 'Informations : [', $e->getCode(), '] ', $e->getMessage(); // On affiche le n° de l'erreur ainsi que le message.
    }


     /* La classe à instancier ici est celle qui doit l'être lorsqu'un paramètre est invalide. On regarde la documentation,
        et on tombe surInvalidArgumentException. Le code donnerait donc : */

    function quatrièmeAddition($a, $b) {

        if (!is_numeric($a) || !is_numeric($b)) {

            throw new InvalidArgumentException('Les deux paramètres doivent être des nombres');
        }
        
        return $a + $b;
    }

    echo quatrièmeAddition(12, 3), '<br />';
    echo quatrièmeAddition('azerty', 54), '<br />';
    echo quatrièmeAddition(4, 8);

    // Cela permet de mieux se repérer dans le code et surtout de mieux cibler les erreurs grâce aux multiples blocs catch.


    //// Exécuter un code même si l'exception n'est pas attrapée ////

    /* Si jamais une exception est lancée dans un bloc try mais non attrapée dans un bloc catch , alors une erreur fatale sera levée
       car l'exception ne sera pas attrapée. Il existe une façon d'exécuter du code avant de lever l'erreur fatale grâce au bloc finally. */

    $db = new PDO('mysql:host=localhost;dbname=tests', 'root', '');
    
    try {

        // Quelques opérations sur la base de données
    }
    finally {

        echo 'Action effectuée quoi qu\'il arrive';
    }

    /* Dans le bloc finally , on trouvera généralement des opérations de nettoyage, comme la fermeture d'un fichier ou d'une connexion
       par exemple. Cela permet de s'assurer que le script se terminera plus ou moins comme il faut. */



    //// Gérer les erreurs facilement ////

    // Convertir les erreurs en exceptions //


    class MonExceptionTrois extends ErrorException {

        public function __toString() {

            switch ($this->severity) {

                case E_USER_ERROR : // Si l'utilisateur émet une erreur fatale;
                    $type = 'Erreur fatale';
                    break;

                case E_WARNING : // Si PHP émet une alerte.

                case E_USER_WARNING : // Si l'utilisateur émet une alerte.
                    $type = 'Attention';
                    break;
                
                case E_NOTICE : // Si PHP émet une notice.

                case E_USER_NOTICE : // Si l'utilisateur émet une notice.
                    $type = 'Note';
                    break;

                default : // Erreur inconnue.
                    $type = 'Erreur inconnue';
                    break;
            }

            return '<strong>' . $type . '</strong> : [' . $this->code . '] ' . $this->message . '<br /><strong>' .
                        $this->file . '</strong> à la ligne <strong>' . $this->line . '</strong>';
        }
    }

    function error2exceptionune($code, $message, $fichier, $ligne) {

        // Le code fait office de sévérité.
        // Reportez-vous aux constantes prédéfinies pour en savoir plus.
        // http://fr2.php.net/manual/fr/errorfunc.constants.php
        throw new MonExceptionTrois($message, 0, $code, $fichier, $ligne);
    }

    set_error_handler('error2exceptionune');
    // set_error_handler permet d'enregistrer une fonction en callback qui sera appelée à chaque fois que l'une de ces trois erreurs sera lancée



    //// Personnaliser les exceptions non attrapées ////

    /* Nous avons réussi à transformer toutes nos erreurs en exceptions en les interceptant grâce à set_error_handler.
       Étant donné que la moindre erreur lèvera une exception, il serait intéressant de personnaliser l'erreur générée par PHP.
       C'est qu'une exception non attrapée génère une longue et laide erreur fatale. Nous allons donc, comme pour les erreurs,
       intercepter les exceptions grâce à set_exception_handler. Cette fonction demande un seul argument :
       le nom de la fonction à appeler lorsqu'une exception est lancée. La fonction de callback doit accepter un argument :
       c'est un objet représentant l'exception. */


    class MonExceptionQuatre extends ErrorException {

        public function __toString() {

            switch ($this->severity) {

                case E_USER_ERROR : // Si l'utilisateur émet une erreur fatale.
                    $type = 'Erreur fatale';
                    break;

                case E_WARNING : // Si PHP émet une alerte.

                case E_USER_WARNING : // Si l'utilisateur émet une alerte.
                    $type = 'Attention';
                    break;

                case E_NOTICE : // Si PHP émet une notice.

                case E_USER_NOTICE : // Si l'utilisateur émet une notice.
                    $type = 'Note';
                    break;

                default : // Erreur inconnue.
                    $type = 'Erreur inconnue';
                    break;
            }

        return '<strong>' . $type . '</strong> : [' . $this->code . '] ' . $this->message . '<br /><strong>' . $this->file . '</strong> à la ligne <strong>' . $this->line . '</strong>';
        }
    }

    function error2exception($code, $message, $fichier, $ligne) {

        // Le code fait office de sévérité.
        // Reportez-vous aux constantes prédéfinies pour en savoir plus.
        // http://fr2.php.net/manual/fr/errorfunc.constants.php
        throw new MonExceptionQuatre($message, 0, $code, $fichier, $ligne);
    }

    function customException($e) {

        echo 'Ligne ', $e->getLine(), ' dans ', $e->getFile(), '<br /><strong>Exception lancée</strong> : ', $e->getMessage();
    }

    set_error_handler('error2exception');
    set_exception_handler('customException');

    /* L'exception est bien interceptée et non attrapée ! Cela signifie que l'on attrape l'exception, qu'on effectue des opérations
       puis qu'on la relâche. Le script, une fois customException appelé, est automatiquement interrompu. */

    /* Ne jamais lancer d'exception dans le gestionnaire d'exception (ici customException). En effet, cela créerait une boucle
       infinie puisque le gestionnaire lance lui-même une exception. L'erreur lancée est la même que celle vue précédemment :
       il s'agit de l'erreur fatale « Exception thrown without a stack frame in Unknown on line 0 ». */

?>
