
<?php

    /* une interface est une classe entièrement abstraite. Son rôle est de décrire un comportement à notre objet.
       Les interfaces ne doivent pas être confondues avec l'héritage :
       l'héritage représente un sous-ensemble (exemple : un magicien est un sous-ensemble d'un personnage).
       Ainsi, une voiture et un personnage n'ont aucune raison d'hériter d'une même classe.

       Une voiture et un personnage n'ont aucune raison d'hériter d'une même classe. Par contre, une voiture et un personnage
       peuvent tous les deux se déplacer, donc une interface représentant ce point commun pourra être créée. */


    interface Movable {

         public function move($dest);

         /* Toutes les méthodes présentes dans une interface doivent être publiques.
            Une interface ne peut pas lister de méthodes abstraites ou finales.
            Une interface ne peut pas avoir le même nom qu'une classe et vice-versa. */

    }

    class Personnage implements Movable {

        public function move($dest) {

            
        }
    }

    // Implémenter plusieurs interfaces par classe, à condition que celles-ci n'aient aucune méthode portant le même nom.



    interface iA {

        public function test1();
    }

    interface iB {

        public function test2();
    }

    class A implements iA, iB {

        // Pour ne générer aucune erreur, il va falloir écrire les méthodes de iA et de iB.
        
        public function test1() {

        }
        
        public function test2() {

        }
    }



    //// LES CONSTANTES D'INTERFACES ////

    /* Les constantes d'interfaces fonctionnent exactement comme les constantes de classes.
       Elles ne peuvent être écrasées par des classes qui implémentent l'interface. */


    interface iInterface {

        const MA_CONSTANTE = 'Hello !';
    }

    echo iInterface::MA_CONSTANTE; // Affiche Hello !

    class MaClasse implements iInterface {


    }

    echo MaClasse::MA_CONSTANTE; // Affiche Hello !



    //// HÉRITER SES INTERFACES ////

    /* Comme pour les classes, vous pouvez hériter vos interfaces grâce à l'opérateur extends.
       Vous ne pouvez réécrire ni une méthode ni une constante qui a déjà été listée dans l'interface parente. */

    interface iC {

        public function test1();
    }

    interface iD extends iC {

        public function test1 ($param1, $param2); // Erreur fatale : impossible de réécrire cette méthode.
    }

    interface iE extends iC {

        public function test2();
    }

    class MaClasseTrois implements iE {

        // Pour ne générer aucune erreur, on doit écrire les méthodes de iC et aussi de iA.
        
        public function test1() {

        }

        public function test2() {

        }
    }

    /* Contrairement aux classes, les interfaces peuvent hériter de plusieurs interfaces à la fois.
       Il vous suffit de séparer leur nom par une virgule. 

       Si une interface (iC) implémente deux autres interfaces (iA, iB),
       une classe créée (iD) impémentera également les interfaces iA et iB */



    //// INTERFACES PRÉDÉFINIES ////

    /* Définition d'un itérateur :
        Un itérateur est un objet capable de parcourir un autre objet. Cet objet à parcourir doit pouvoir être parcouru
        (on dit alors qu'il doit être itératif). Pour ce faire, nous devons imposer un comportement à nos objets afin qu'ils puissent
        être parcourus. Ce comportement à imposer se fera par le biais d'interfaces !
        L'interface la plus basique pour rendre un objet itératif est Iterator . */

    // L'interface Iterator //

    // Si votre classe implémente cette interface, alors vous pourrez modifier le comportement de votre objet lorsqu'il est parcouru. */

    /** Cette interface comporte 5 méthodes :

     * current : renvoie l'élément courant;
     * key :     retourne la clé de l'élément courant;
     * next :    déplace le pointeur sur l'élément suivant;
     * rewind :  remet le pointeur sur le premier élément;
     * valid :   vérifie si la position courante est valide;*/ 

    /* En écrivant ces méthodes, on pourra renvoyer la valeur qu'on veut, et pas forcément la valeur de l'attribut actuellement lu.
       Imaginons qu'on ait un attribut qui soit un tableau. On pourrait très bien créer un petit script qui, au lieu de parcourir l'objet,
       parcourt le tableau ! Vous aurez besoin d'un attribut $position qui stocke la position actuelle. */

    class MaClasseQuatre implements Iterator {

        private $position = 0;
        private $tableau = ['Premier élément', 'Deuxième élément', 'Troisième élément', 'Quatrième élément', 'Cinquième élément'];

        // Retourne l'élément courant du tableau.
        public function current() {

            return $this->tableau[$this->position];
        }

        // Retourne la clé actuelle (c'est la même que la position dans notre cas).
        public function key() {

            return $this->position;
        }

        // Déplace le curseur vers l'élément suivant.
        public function next() {

            $this->position++;
        }

        // Remet la position du curseur à 0.
        public function rewind() {

            $this->position = 0;
        }

        // Permet de tester si la position actuelle est valide.
        public function valid() {

            return isset($this->tableau[$this->position]);
        }
    }

    $objet = new MaClasseQuatre;

    foreach ($objet as $key => $value) {

        echo $key, ' => ', $value, '<br />';
    }

    /* Résultat : 

            0 => Premier élément
            1 => Deuxième élément
            2 => Troisième élément
            3 => Quatrième élément
            4 => Cinquième élément
    */


    // L'interface SeekableIterator //

    /* SeekableIterator ajoute une méthode à la liste des méthodes d'Iterator: la méthode seek. Cette méthode permet de placer le
       curseur interne à une position précise. Elle demande donc un argument : la position du curseur à laquelle il faut le placer.
       Je vous déconseille de modifier directement l'attribut $position afin d'assigner directement la valeur de l'argument à $position.
       En effet, qui vous dit que la valeur de l'argument est une position valide ? */

    class MaClasseCinq implements SeekableIterator {

        private $position = 0;
        private $tableau = ['Premier élément', 'Deuxième élément', 'Troisième élément', 'Quatrième élément', 'Cinquième élément'];

        // Retourne l'élément courant du tableau.
        public function current() {

            return $this->tableau[$this->position];
        }

        // Retourne la clé actuelle (c'est la même que la position dans notre cas).
        public function key() {

            return $this->position;
        }

        // Déplace le curseur vers l'élément suivant.
        public function next() {

            $this->position++;
        }

        // Remet la position du curseur à 0.
        public function rewind() {

            $this->position = 0;
        }

        // Déplace le curseur interne.
        public function seek($position) {

            $anciennePosition = $this->position;
            $this->position = $position;

            if (!$this->valid()) {

                trigger_error('La position spécifiée n\'est pas valide', E_USER_WARNING);
                $this->position = $anciennePosition;
            }
        }

        // Permet de tester si la position actuelle est valide.
        public function valid() {

            return isset($this->tableau[$this->position]);
        }
    }

        $objet = new MaClasseCinq;

        foreach ($objet as $key => $value) {

            echo $key, ' => ', $value, '<br />';
        }

        $objet->seek(2);
        echo '<br />', $objet->current();

        /* Résultat : 

                0 => Premier élément
                1 => Deuxième élément
                2 => Troisième élément
                3 => Quatrième élément
                4 => Cinquième élément

                Troisième élément
        */



    // L'interface ArrayAccess //

    /* Nous allons enfin, grâce à cette interface, pouvoir placer des crochets à la suite de notre objet avec la clé à laquelle accéder,
       comme sur un vrai tableau ! L'interface ArrayAccess liste quatre méthodes :

    - offsetExists: méthode qui vérifiera l'existence de la clé entre crochets lorsque l'objet est passé à la fonction
        issetouempty(cette valeur entre crochet est passée à la méthode en paramètre).

    - offsetGet: méthode appelée lorsqu'on fait un simple $obj['clé']. La valeur 'clé' est donc passée à la méthode offsetGet.

    - offsetSet: méthode appelée lorsqu'on assigne une valeur à une entrée. Cette méthode reçoit donc deux arguments,
        la valeur de la clé et la valeur qu'on veut lui assigner.

    - offsetUnset: méthode appelée lorsqu'on appelle la fonction unset sur l'objet avec une valeur entre crochets.
        Cette méthode reçoit un argument, la valeur qui est mise entre les crochets.

    Nous allons maintenant implémenter cette interface et gérer l'attribut $tableau grâce aux quatre méthodes. */

    class MaClasseSix implements SeekableIterator, ArrayAccess {

        private $position = 0;
        private $tableau = ['Premier élément', 'Deuxième élément', 'Troisième élément', 'Quatrième élément', 'Cinquième élément'];


        /* MÉTHODES DE L'INTERFACE SeekableIterator */


        // Retourne l'élément courant du tableau.
        public function current() {

            return $this->tableau[$this->position];
        }

        // Retourne la clé actuelle (c'est la même que la position dans notre cas).
        public function key() {

            return $this->position;
        }

        // Déplace le curseur vers l'élément suivant.
        public function next() {

            $this->position++;
        }

        // Remet la position du curseur à 0.
        public function rewind() {

            $this->position = 0;
        }

        // Déplace le curseur interne.
        public function seek($position) {

            $anciennePosition = $this->position;
            $this->position = $position;

            if (!$this->valid()) {

                trigger_error('La position spécifiée n\'est pas valide', E_USER_WARNING);
                $this->position = $anciennePosition;
            }
        }

        // Permet de tester si la position actuelle est valide.
        public function valid() {

            return isset($this->tableau[$this->position]);
        }


        /* MÉTHODES DE L'INTERFACE ArrayAccess */



        // Vérifie si la clé existe.
        public function offsetExists($key) {

            return isset($this->tableau[$key]);
        }

        // Retourne la valeur de la clé demandée.
        // Une notice sera émise si la clé n'existe pas, comme pour les vrais tableaux.
        public function offsetGet($key) {

            return $this->tableau[$key];
        }

        // Assigne une valeur à une entrée.
        public function offsetSet($key, $value) {

            $this->tableau[$key] = $value;
        }

        // Supprime une entrée et émettra une erreur si elle n'existe pas, comme pour les vrais tableaux.
        public function offsetUnset($key) {

            unset($this->tableau[$key]);
        }
    }

    $objet = new MaClasseSix;

    echo 'Parcours de l\'objet...<br />';

    foreach ($objet as $key => $value) {

        echo $key, ' => ', $value, '<br />';
    }

    echo '<br />Remise du curseur en troisième position...<br />';
    $objet->seek(2);
    echo 'Élément courant : ', $objet->current(), '<br />';

    echo '<br />Affichage du troisième élément : ', $objet[2], '<br />';
    echo 'Modification du troisième élément... ';
    $objet[2] = 'Hello world !';
    echo 'Nouvelle valeur : ', $objet[2], '<br /><br />';

    echo 'Destruction du quatrième élément...<br />';
    unset($objet[3]);

    if (isset($objet[3])) {

        echo '$objet[3] existe toujours... Bizarre...';
    }
    else {

        echo 'Tout se passe bien, $objet[3] n\'existe plus !';
    }

    /* Résultat :

    Parcours de l'objet...

        0 => Premier élément
        1 => Deuxième élément
        2 => Troisième élément
        3 => Quatrième élément
        4 => Cinquième élément

        Remise du curseur en troisième position...
        Elément courant : Troisième élément

        Affichage du troisième élément : Troisième élément
        Modification du troisième élément... Nouvelle valeur : Hello World !

        Destruction du quatrième élément...
        Tout se passe bien, $objet[3] n'existe plus !

    */

    // On se rapproche vraiment du comportement d'un tableau. On peut faire tout ce qu'on veut, comme sur un tableau !



    // L'interface Countable //



    class MaClasseSept implements SeekableIterator, ArrayAccess, Countable {

        private $position = 0;
        private $tableau = ['Premier élément', 'Deuxième élément', 'Troisième élément', 'Quatrième élément', 'Cinquième élément'];


        /* MÉTHODES DE L'INTERFACE SeekableIterator */


        // Retourne l'élément courant du tableau.
        public function current() {

            return $this->tableau[$this->position];
        }

        // Retourne la clé actuelle (c'est la même que la position dans notre cas).
        public function key() {

            return $this->position;
        }

        // Déplace le curseur vers l'élément suivant.
        public function next() {

            $this->position++;
        }

        // Remet la position du curseur à 0.
        public function rewind() {

            $this->position = 0;
        }

        // Déplace le curseur interne.
        public function seek($position) {

            $anciennePosition = $this->position;
            $this->position = $position;
            
            if (!$this->valid()) {

                trigger_error('La position spécifiée n\'est pas valide', E_USER_WARNING);
                $this->position = $anciennePosition;
            }
        }

        // Permet de tester si la position actuelle est valide.
        public function valid() {

            return isset($this->tableau[$this->position]);
        }


        /* MÉTHODES DE L'INTERFACE ArrayAccess */


        // Vérifie si la clé existe.
        public function offsetExists($key) {

            return isset($this->tableau[$key]);
        }

        // Retourne la valeur de la clé demandée.
        // Une notice sera émise si la clé n'existe pas, comme pour les vrais tableaux.
        public function offsetGet($key) {

            return $this->tableau[$key];
        }

        // Assigne une valeur à une entrée.
        public function offsetSet($key, $value) {

            $this->tableau[$key] = $value;
        }

        // Supprime une entrée et émettra une erreur si elle n'existe pas, comme pour les vrais tableaux.
        public function offsetUnset($key) {

            unset($this->tableau[$key]);
        }


        /* MÉTHODES DE L'INTERFACE Countable */


        // Retourne le nombre d'entrées de notre tableau.
        public function count() {

            return count($this->tableau);
        }
    }

    $objet = new MaClasseSept;

    echo 'Parcours de l\'objet...<br />';

    foreach ($objet as $key => $value) {

        echo $key, ' => ', $value, '<br />';
    }

    echo '<br />Remise du curseur en troisième position...<br />';
    $objet->seek(2);
    echo 'Élément courant : ', $objet->current(), '<br />';

    echo '<br />Affichage du troisième élément : ', $objet[2], '<br />';
    echo 'Modification du troisième élément... ';
    $objet[2] = 'Hello world !';
    echo 'Nouvelle valeur : ', $objet[2], '<br /><br />';

    echo 'Actuellement, mon tableau comporte ', count($objet), ' entrées<br /><br />';

    echo 'Destruction du quatrième élément...<br />';
    unset($objet[3]);

    if (isset($objet[3])) {

        echo '$objet[3] existe toujours... Bizarre...';
    }
    else {

        echo 'Tout se passe bien, $objet[3] n\'existe plus !';
    }

    echo '<br /><br />Maintenant, il n\'en comporte plus que ', count($objet), ' !';

    /* Résultat :

    Parcours de l'objet...

        0 => Premier élément
        1 => Deuxième élément
        2 => Troisième élément
        3 => Quatrième élément
        4 => Cinquième élément

        Remise du curseur en troisième position...
        Elément courant : Troisième élément

        Affichage du troisième élément : Troisième élément
        Modification du troisième élément... Nouvelle valeur : Hello World !

        Destruction du quatrième élément...
        Tout se passe bien, $objet[3] n'existe plus !

        Maintenant, il n'en comporte plus que 4 !

    */



    // Bonus: la classe arrayIterator //

    /* La classe que nous venons de créer pour pouvoir créer des « objets-tableaux » existe déjà. En effet, PHP possède nativement
       une classe nommée ArrayIterator. Comme notre précédente classe, celle-ci implémente les quatre interfaces qu'on a vues.

       Sachez que réécrire des classes ou fonctions natives de PHP est un excellent exercice (et c'est valable pour tous les langages
       de programmation). Je ne vais pas m'attarder sur cette classe, étant donné qu'elle s'utilise exactement comme la nôtre.
       Elle possède les mêmes méthodes, à une différence près : cette classe implémente un constructeur qui accepte un tableau en guise
       d'argument. C'est ce tableau qui sera « transformé » en objet. Ainsi, si vous faites un echo $monInstanceArrayIterator['cle'],
       alors à l'écran s'affichera l'entrée qui a pour clé cle du tableau passé en paramètre. */

?>
