
<?php

    //// __destruct ////
    class Detruire {

        public function __destruct() { // Destruction de l'objet

            echo 'Destruction de Detruire';
        }
    }

    $detruire = new Detruire;


    //// SURCHARGE MAGIQUE DES ATTRIBUTS ET MÉTHODES ////

    /* La surcharge magique d'attributs ou méthodes consiste à créer dynamiquement des attributs et méthodes.
       Cela est possible lorsque l'on tente d'accéder à un élément qui n'existe pas ou auquel on n'a pas accès
       (s'il est privé et qu'on tente d'y accéder depuis l'extérieur de la classe par exemple).
       Dans ce cas-là, on a 6 méthodes magiques */


    //// __set ////

    /* Cette méthode est appelée lorsque l'on essaye d'assigner une valeur à un attribut auquel on n'a pas accès ou qui n'existe pas.
       Cette méthode prend deux paramètres : le premier est le nom de l'attribut auquel on a tenté d'assigner une valeur,
       le second paramètre est la valeur que l'on a tenté d'assigner à l'attribut. Cette méthode ne retourne rien.
       Vous pouvez simplement faire ce que bon vous semble ! */

    class Set {

    private $unAttributPrive;
    
    public function __set($nom, $valeur) {

        echo 'Ah, on a tenté d\'assigner à l\'attribut <strong>', $nom, '</strong> la valeur <strong>', $valeur, '</strong> mais c\'est pas possible !<br />';
    }
    }

    $obj = new Set;

    $obj->attribut = 'Simple test';
    $obj->unAttributPrive = 'Autre simple test';
    /* Résultat : Ah, on a tenté d'assigner à l'attribut *attribut* la valeur *Simple test* mais c'est pas possible !
                  Ah, on a tenté d'assigner à l'attribut *unAttributPrive* la valeur *Autre simple test* mais c'est pas possible ! */


    // On stocke dans un tableau tous les attributs (avec leurs valeurs) que nous avons essayé de modifier ou créer
    class SetTableau {

        private $attributs = [];
        private $unAttributPrive;

        public function __set($nom, $valeur) {

            $this->attributs[$nom] = $valeur;
        }

        public function afficherAttributs() {

            echo '<pre>', print_r($this->attributs, true), '</pre>';
        }
    }

    $obj = new SetTableau;
    
    $obj->attribut = 'Simple test';
    $obj->unAttributPrive = 'Autre simple test';

    $obj->afficherAttributs();



    //// __get ////

    /* Cette méthode est appelée lorsque l'on essaye d'accéder à un attribut qui n'existe pas ou auquel on n'a pas accès.
       Elle prend un paramètre : le nom de l'attribut auquel on a essayé d'accéder.
       Cette méthode peut retourner ce qu'elle veut (ce sera, en quelque sorte, la valeur de l'attribut inaccessible). */

    class Get {

        private $unAttributPrive;

        public function __get($nom) {

            return 'Impossible d\'accéder à l\'attribut <strong>' . $nom . '</strong>, désolé !<br />';
        }
    }

    $obj = new Get;

    echo $obj->attribut;
    echo $obj->unAttributPrive;
    /* Résultat : Impossible d'accéder à l'attribut *attribut*, désolé !
                  Impossible d'accéder à l'attribut *unAttributPrive*, désolé ! */


    /* On vérifie si l'attribut auquel on a tenté d'accéder est contenu dans le tableau de stockage d'attributs.
       Si tel est le cas, on l'affiche, sinon, on ne fait rien. */

    class GetTableau {

        private $attributs = [];
        private $unAttributPrive;

        public function __get($nom) {

            if (isset($this->attributs[$nom])) {

                return $this->attributs[$nom];
            }
        }

        public function __set($nom, $valeur) {

            $this->attributs[$nom] = $valeur;
        }

        public function afficherAttributs() {

            echo '<pre>', print_r($this->attributs, true), '</pre>';
        }
    }
    
    $obj = new GetTableau;
    
    $obj->attribut = 'Simple test';
    $obj->unAttributPrive = 'Autre simple test';
    
    echo $obj->attribut;
    echo $obj->autreAtribut;



    //// __isset ////

    /* __isset est appelée lorsque l'on appelle la fonction isset sur un attribut qui n'existe pas ou auquel on n'a pas accès.
       Étant donné que la fonction initiale isset renvoie true ou false, la méthode magique __isset doit renvoyer un booléen.
       Cette méthode prend un paramètre : le nom de l'attribut que l'on a envoyé à la fonction isset.
       Vous pouvez par exemple utiliser la classe précédente en implémentant la méthode __isset, ce qui peut nous donner : */

    class issetMagique {

        private $attributs = [];
        private $unAttributPrive;

        public function __set($nom, $valeur) {

            $this->attributs[$nom] = $valeur;
        }

        public function __get($nom) {

            if (isset($this->attributs[$nom])) {

                return $this->attributs[$nom];
            }
        }

        public function __isset($nom) {

            return isset($this->attributs[$nom]);
        }
    }

    $obj = new issetMagique;

    $obj->attribut = 'Simple test';
    $obj->unAttributPrive = 'Autre simple test';

    if (isset($obj->attribut)) {

        echo 'L\'attribut <strong>attribut</strong> existe !<br />';
    }
    else {

        echo 'L\'attribut <strong>attribut</strong> n\'existe pas !<br />';
    }

    if (isset($obj->unAutreAttribut)) {

        echo 'L\'attribut <strong>unAutreAttribut</strong> existe !';
    }
    else {

        echo 'L\'attribut <strong>unAutreAttribut</strong> n\'existe pas !';
    }

    /* Résultat : L'attribut *atrribut* existe ! 
                  L'attribut *un AutreAttribut existe* !*/



    //// __unset ////

    /* Cette méthode est appelée lorsque l'on tente d'appeler la fonction unset sur un attribut inexistant ou auquel on n'a pas accès.
       On peut facilement implémenter __unset à la classe précédente de manière à supprimer l'entrée correspondante
       dans notre tableau $attributs. Cette méthode ne doit rien retourner. */

    class unsetMagique {

        private $attributs = [];
        private $unAttributPrive;

        public function __set($nom, $valeur) {

            $this->attributs[$nom] = $valeur;
        }

        public function __get($nom) {

            if (isset($this->attributs[$nom])) {

                return $this->attributs[$nom];
            }
        }

        public function __isset($nom) {

            return isset($this->attributs[$nom]);
        }

        public function __unset($nom) {

            if (isset($this->attributs[$nom])) {

                unset($this->attributs[$nom]);
            }
        }
    }

    $obj = new unsetMagique;

    $obj->attribut = 'Simple test';
    $obj->unAttributPrive = 'Autre simple test';

    if (isset($obj->attribut)) {

        echo 'L\'attribut <strong>attribut</strong> existe !<br />';
    }
    else {

        echo 'L\'attribut <strong>attribut</strong> n\'existe pas !<br />';
    }

    unset($obj->attribut);

    if (isset($obj->attribut)) {

        echo 'L\'attribut <strong>attribut</strong> existe !<br />';
    }
    else {

        echo 'L\'attribut <strong>attribut</strong> n\'existe pas !<br />';
    }

    if (isset($obj->unAutreAttribut)) {

        echo 'L\'attribut <strong>unAutreAttribut</strong> existe !';
    }
    else {

        echo 'L\'attribut <strong>unAutreAttribut</strong> n\'existe pas !';
    }

    /* Résultat : L'attribut *attribut* existe !
                  L'attribut *attribut* n'existe pas !
                  L'attribut *unAutreAttribut* n'existe pas ! */



    //// __call ////

    /* Parlons cette fois-ci des méthodes que l'on appelle alors qu'on n'y a pas accès (soit elle n'existe pas, soit elle est privée).
       La méthode __call sera appelée lorsque l'on essayera d'appeler une telle méthode. Elle prend deux arguments :
       le premier est le nom de la méthode que l'on a essayé d'appeler et le second est la liste des arguments qui lui ont été passés
       (sous forme de tableau). */

    class call {

        public function __call($nom, $arguments) {

            echo 'La méthode <strong>', $nom, '</strong> a été appelée alors qu\'elle n\'existe pas !
                 Ses arguments étaient les suivants : <strong>', implode($arguments), '</strong>';
        }
    }

    $obj = new call;

    $obj->methode(123, 'test');

    /* Résultat : La méthode *methode* a été appelée alors qu'elle n'existe pas !
                  Ses arguments étaient les suivants : 123, test */



    //// __callStatic ////

    /* Et si on essaye d'appeler une méthode qui n'existe pas statiquement ? Et bien, erreur fatale ! Sauf si vous utilisez __callStatic.
       Cette méthode est appelée lorsque vous appelez une méthode dans un contexte statique alors qu'elle n'existe pas.
       La méthode magique __callStatic doit obligatoirement être static! */

    class callStatic {

        public function __call($nom, $arguments) {

            echo 'La méthode <strong>', $nom, '</strong> a été appelée alors qu\'elle n\'existe pas !
                 Ses arguments étaient les suivants : <strong>', implode ($arguments), '</strong><br />';
        }
        
        public static function __callStatic($nom, $arguments) {

            echo 'La méthode <strong>', $nom, '</strong> a été appelée dans un contexte statique alors qu\'elle n\'existe pas !
                 Ses arguments étaient les suivants : <strong>', implode ($arguments), '</strong><br />';
        }
    }

    $obj = new callStatic;

    $obj->methode(123, 'test');

    callStatic::methodeStatique(456, 'autre test');

    /* Résultat : La méthode *methode* a été appelée alors qu'elle n'existe pas !
                  Ses arguments étaient les suivants : 123, test
                  La méthode *methode* a été appelée alors qu'elle n'existe pas !
                  Ses arguments étaient les suivants : 456, autre test */



    //// LINÉARISER SES OBJETS ////

    /* La linéarisation est une technique pratique pour stocker ou passer des valeurs de PHP entre scripts, sans perdre ni leur structure,
       ni leur type. Par fois on veut pouvoir enregistrer un array en intégralité (c'est-à-dire avec les clefs associées aux valeurs).
       Plusieurs possibilités pour réaliser ce travail. En effet une des possibilités est l’utilisation de la fonction particulière
       et puissante dans PHP et depuis la version 4: serialize(). Cette fonction peut coder une variable, quel que soit son type,
       en une chaîne de caractères, il s’agit d’un codage spéciale.

       Pour récupérer une variable linéarisée, et retrouver la variable normal, on utilise la fonction  unserialize().  */

    /* Création de l'objet ($objetConnexion = new Connexion;) ;
        transformation de l'objet en chaîne de caractères ($_SESSION['connexion'] = serialize($objetConnexion);) ;
        changement de page ;
        transformation de la chaîne de caractères en objet ($objetConnexion = unserialize($_SESSION['connexion']);). */

    /* Les fonctions serialize et unserialize ne se contentent pas de transformer le paramètre qu'on leur passe en autre chose :
       elles vérifient si, dans l'objet passé en paramètre (pour serialize), il y a une méthode__sleep, auquel cas elle est exécutée.
       Si unserialize est appelée, la fonction vérifie si l'objet obtenu comporte une méthode __wakeup, auquel cas elle est appelée. */


    //// serialize ET __sleep ////

    /* La méthode magique __sleep est utilisée pour nettoyer l'objet ou pour sauver des attributs. Si la méthode magique __sleep
       n'existe pas, tous les attributs seront sauvés. Cette méthode doit renvoyer un tableau avec les noms des attributs à sauver.
       Par exemple, si vous voulez sauver $serveur et $login, la fonction devra retourner ['serveur', 'login']; */

    class Connexion {

        protected $pdo, $serveur, $utilisateur, $motDePasse, $dataBase;

        public function __construct($serveur, $utilisateur, $motDePasse, $dataBase) {

            $this->serveur = $serveur;
            $this->utilisateur = $utilisateur;
            $this->motDePasse = $motDePasse;
            $this->dataBase = $dataBase;

            $this->connexionBDD();
        }

        protected function connexionBDD() {

            $this->pdo = new PDO('mysql:host='.$this->serveur.';dbname='.$this->dataBase, $this->utilisateur, $this->motDePasse);
        }

        public function __sleep() {

            // Ici sont à placer des instructions à exécuter juste avant la linéarisation.
            // On retourne ensuite la liste des attributs qu'on veut sauver.
            return ['serveur', 'utilisateur', 'motDePasse', 'dataBase'];
        }

        public function __wakeup() {

            $this->connexionBDD();
        }
    }

    // ?????????? Dans un autre fichier ??????????
    session_start();

    if (!isset($_SESSION['connexion'])) {

        $connexion = new Connexion('localhost', 'root', '', 'tests');
        $_SESSION['connexion'] = $connexion;
    
        echo 'Actualisez la page !';
    }
    else {

        echo '<pre>';
        var_dump($_SESSION['connexion']); // On affiche les infos concernant notre objet.
        echo '</pre>';
    }

    // Notre objet a bel et bien été sauvegardé comme il fallait, et que tous les attributs ont été sauvés

    /* Étant donné que notre objet est restauré automatiquement lors de l'appel de session_start(),
       la classe correspondante doit être déclarée avant, sinon l'objet désérialisé sera une instance de __PHP_Incomplete_Class_Name,
       classe qui ne contient aucune méthode (cela produira donc un objet inutile).
       Si vous avez un autoload qui chargera la classe automatiquement, il sera appelé. */



    //// AUTRE METHODE MAGIQUE ////

    //// __toString ////

    /* La méthode magique __toStringest appelée lorsque l'objet est amené à être converti en chaîne de caractères.
       Cette méthode doit retourner la chaîne de caractères souhaitée. */

    class toString {

        protected $texte;

        public function __construct($texte) {

            $this->texte = $texte;
        }

        public function __toString() {

            return $this->texte;
        }
    }

    $obj = new toString('Hello world !');

    // Solution 1 : le cast
    $texte = (string) $obj;
    var_dump($texte); // Affiche : string(13) "Hello world !".

    // Solution 2 : directement dans un echo
    echo $obj; // Affiche : Hello world !



    //// __set_state ////

    /* La méthode magique __set_state est appelée lorsque vous appelez la fonction var_export en passant votre objet à exporter en paramètre.
       Cette fonction var_export a pour rôle d'exporter la variable passée en paramètre sous forme de code PHP (chaîne de caractères).
       Si vous ne spécifiez pas de méthode __set_state dans votre classe, une erreur fatale sera levée. */

    /* Notre méthode __set_state prend un paramètre, la liste des attributs ainsi que leur valeur dans un tableau associatif
       (['attribut' => 'valeur']). Notre méthode magique devra retourner l'objet à exporter. Il faudra donc créer un nouvel objet
       et lui assigner les valeurs qu'on souhaite, puis le retourner. */

    // Ne jamais retourner $this, car cette variable n'existera pas dans cette méthode ! var_export reportera donc une valeur nulle.

    /* Puisque la fonction var_export retourne du code PHP valide, on peut utiliser la fonction eval qui exécute du code PHP
       sous forme de chaîne de caractères qu'on lui passe en paramètre. */

    class Export {

        protected $chaine1, $chaine2;

        public function __construct($param1, $param2) {

            $this->chaine1 = $param1;
            $this->chaine2 = $param2;
        }

        public function __set_state($valeurs) { // Liste des attributs de l'objet en paramètre.

            $obj = new Export($valeurs['chaine1'], $valeurs['chaine2']); // On crée un objet avec les attributs de l'objet que l'on veut exporter.
            return $obj; // on retourne l'objet créé.
        }
    }

    $obj1 = new Export('Hello ', 'world !');

    eval('$obj2 = ' . var_export($obj1, true) . ';'); // On crée un autre objet, celui-ci ayant les mêmes attributs que l'objet précédent.

    echo '<pre>', print_r($obj2, true), '</pre>';

    /* Résultat : Export Objet {

                      [chaine1:protected] => Hello
                      [chaine2:protected] => World !
                  } */



    //// __invoke ////

    // Sert à utiliser un objet comme une fonction.

    $obj = new Get();
    $obj('Petit test'); // Utilisation de l'objet comme fonction.

    // Résultat : Erreur fatale.

    class invoke {

        public function __invoke($argument) { // __invoke est appelée dès qu'on essaye d'utiliser l'objet comme fonction.

            echo $argument;
        }
    }

    $obj = new invoke;

    $obj(5); // Affiche « 5 ».



    //// __debugInfo ////

    /* Cette méthode magique est invoquée sur notre objet lorsqu'on appelle la fonction var_dump. Si on lui donne un objet,
       var_dump va afficher les détails de tous les attributs de l'objet, qu'ils soient publics, protégés ou privés.
       La méthode magique __debugInfo permet de modifier ce comportement en ne sélectionnant que les attributs à afficher
       ainsi que ce qu'il faut afficher. Pour ce faire, cette méthode renverra sous forme de tableau associatif
       la liste des attributs à afficher avec leurs valeurs. */

    class FileReader {

        protected $f;

        public function __construct($path) {

            $this->f = fopen($path, 'c+');
        }

        public function __debugInfo() {

            return ['f' => fstat($this->f)];
        }
    }

    $f = new FileReader('fichier.txt');
    var_dump($f); // Affiche les informations retournées par fstat.







?>