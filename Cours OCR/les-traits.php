
<?php

    /* Soit deux classes, Writer et Mailer. La première est chargée d'écrire du texte dans un fichier, tandis que la seconde
       envoie un texte par mail. Cependant, il est agréable de mettre en forme le texte. Pour cela, vous décidez de formater le
       texte en HTML. Or, un problème se pose : vous allez devoir effectuer la même opération (celle de formater en HTML)
       dans deux classes complètement différentes et indépendantes : */

    class WriterOne {

        public function write($text) {

            $text = '<p>Date : '.date('d/m/Y').'</p>'."\n".
                    '<p>'.nl2br($text).'</p>';
            file_put_contents('fichier.txt', $text);
        }
    }


    class MailerOne {

        public function send($text) {

            $text = '<p>Date : '.date('d/m/Y').'</p>'."\n".
                    '<p>'.nl2br($text).'</p>';
            mail('login@fai.tld', 'Test avec les traits', $text);
        }
    }

    // Syntaxe de base //

    /* Les traits sont un moyen d'externaliser du code. Plus précisément, les traits définissent des méthodes
    que les classes peuvent utiliser */

    trait MonTraitUn {

        public function hello() {

            echo 'Hello world !';
        }
    }

    class A {

        use MonTraitUn;
    }

    class B {

        use MonTraitUn;
    }

    $a = new A;
    $a->hello(); // Affiche « Hello world ! ».

    $b = new b;
    $b->hello(); // Affiche aussi « Hello world ! ».

    /* Nous définissons un trait. Un trait n'est autre qu'une mini-classe. Dedans, nous ne déclarons qu'une seule méthode.
       Ensuite, nous déclarons deux classes, chacune utilisant le trait que nous avons créé. L'utilisation d'un trait
       dans une classe se fait grâce au mot-clé use. En utilisant ce mot-clé, toutes les méthodes du trait vont être importées
       dans la classe. Dans le test en fin de fichier, les deux classes possèdent bien une méthode hello()
       et celle-ci affiche bien « Hello world ! ». */



    /* Maintenant, nous allons retirer le gros défaut de notre code : grâce aux traits, nous ferons disparaître la duplication
       de notre code. Nous allons créer un trait qui va contenir une méthode format($text) qui va formater le texte
       passé en argument en HTML. */

    trait HTMLFormater {

        public function format($text) {

            return '<p>Date : '.date('d/m/Y').'</p>'."\n".
                    '<p>'.nl2br($text).'</p>';
        }
    }

    trait TextFormater {

        public function format($text) {

            return '<p>Date : '.date('d/m/Y').'</p>'."\n".$text;
        }
    }

    /* Modification des classes Writer et Mailer afin qu'elles utilisent ce trait pour formater le texte
       qu'elles exploiteront. Utiliser le mot-clé use pour importer toutes les méthodes du trait (ici, il n'y en a qu'une) dans la classe.
       Vous pourrez ainsi utiliser les méthodes comme si elles étaient déclarées dans la classe. */

    class WriterDeux {

        use HTMLFormater;
        
        public function write($text) {

            file_put_contents('fichier.txt', $this->format($text));
        }
    }

    class MailerDeux {

        use HTMLFormater;
        
        public function send($text) {

            mail('login@fai.tld', 'Test avec les traits', $this->format($text));
        }
    }

    $w = new WriterDeux;
    $w->write('Hello world!');

    $m = new MailerDeux;
    $m->send('Hello world!');



    // Utiliser plusieurs traits //

    /* Pour utiliser plusieurs traits, il  suffit de lister tous les traits à utiliser séparés par des virgules */

    //     use HTMLFormater, TextFormater;



    // Résolution des conflits //

    /* Si les traits avaient tous les deux une méthode nommée format() celà produirait une erreur fatale avec le message
       « Trait method format has not been applied, because there are collisions with other trait methods ».
       Pour pallier ce problème, nous pouvons donner une priorité à une méthode d'un trait afin de lui permettre d'écraser
       la méthode de l'autre trait si il y en a une identique. */

    // Exemple avec la classe WriterTrois :


    class WriterTrois {

        use HTMLFormater, TextFormater {

            HTMLFormater::format insteadof TextFormater;
        }

        public function write($text) {

            file_put_contents('fichier.txt', $this->format($text));
        }
    }

    /* Le use est défini dans une paire d'accolades suivant les noms des traits à utiliser. À l'intérieur de cette paire d'accolades
       se trouve la liste des « méthodes prioritaires ». Chaque déclaration de priorité se fait en se terminant par un point-virgule.
       Cette ligne signifie donc :
        « La méthode format() du trait HTMLFormater écrasera la méthode du même nom du trait TextFormater (si elle y est définie). » */



    // Méthodes de traits vs. méthodes de classes //

    /* Si une classe déclare une méthode et qu'elle utilise un trait possédant cette même méthode, alors la méthode déclarée
       dans la classe l'emportera sur la méthode déclarée dans le trait. */

    trait MonTraitDeux {

        public function sayHello() {

        echo 'Hello !';
        }
    }

    class MaClasseUne {

        use MonTraitDeux;
        
        public function sayHello() {

        echo 'Bonjour !';
        }
    }

    $objet = new MaClasseUne;
    $objet->sayHello(); // Affiche « Bonjour ! ».



    // Le trait plus fort que la mère //

    /* À l'inverse, si une classe utilise un trait possédant une méthode déjà implémentée dans la classe mère
       de la classe utilisant le trait, alors ce sera la méthode du trait qui sera utilisée
       (la méthode du trait écrasera celle de la méthode de la classe mère).*/

    trait MonTraitTrois {

        public function speak() {

        echo 'Je suis un trait !';
        }
    }

    class Mere {

        public function speak() {

        echo 'Je suis une classe mère !';
        }
    }

    class Fille extends Mere {

        use MonTraitTrois;
    }

    $fille = new Fille;
    $fille->speak(); // Affiche « Je suis un trait ! »



    //// Plus loin avec les traits ////

    // Définition d'attributs //

    /* Les traits servaient à isoler des méthodes afin de pouvoir les utiliser dans deux classes totalement indépendantes.
       Si le besoin s'en fait sentir, vous pouvez aussi définir des attributs dans votre trait. Ils seront alors à leur tour
       importés dans la classe qui utilisera ce trait. */

    trait MonTraitQuatre {

        protected $attr = 'Hello !';

        public function showAttr() {

            echo $this->attr;
        }
    }

    class MaClasseDeux {

        use MonTraitQuatre;
    }

    $fille = new MaClasseDeux;
    $fille->showAttr();

    /* Une propriété de trait peut être statique. Mais attention, dans ce cas, chaque classe utilisant ce trait aura
       une instance indépendante de cette propriété. */



    // Conflit entre attributs //

    // Si un attribut est défini dans un trait, alors la classe utilisant le trait ne peut pas définir d'attribut possédant le même nom.

    trait MonTraitCinq {

        protected $attr = 'Hello !';
        }

    class MaClasseTrois {

        use MonTraitCinq;

    /*
        protected $attr = 'Hello !';    Lèvera une erreur stricte.
        protected $attr = 'Bonjour !';  Lèvera une erreur fatale.
        private $attr = 'Hello !';      Lèvera une erreur fatale.
    */
    }



    // Traits composés d'autres traits //

    /* Au même titre que les classes, les traits peuvent eux aussi utiliser des traits. La façon de procéder est la même qu'avec les classes,
       tout comme la gestion des conflits entre méthodes. */

    trait C {

        public function saySomething() {

        echo 'Je suis le trait C !';
        }
    }

    trait D {

        use C;
        
        public function saySomethingElse() {

        echo 'Je suis le trait BD !';
        }
    }

    class MaClasseQuatre {

        use D;
    }

    $o = new MaClasseQuatre;
    $o->saySomething(); // Affiche « Je suis le trait C ! »
    $o->saySomethingElse(); // Affiche « Je suis le trait D ! »



    // Changer la visibilité et le nom des méthodes //

    /* Si un trait implémente une méthode, toute classe utilisant ce trait a la capacité de changer sa visibilité, c'est-à-dire la passer en privé, protégé ou public.
       Pour cela, nous allons à nouveau nous servir des accolades qui ont suivi la déclaration de use pour y glisser une instruction. Cette instruction fait appel
       à l'opérateur as, que vous avez déjà peut-être rencontré dans l'utilisation de namespaces. Le rôle est ici le même : créer un alias.
       En effet, vous pouvez aussi changer le nom des méthodes. Dans ce dernier cas, la méthode ne sera pas renommée, mais copiée sous un autre nom,
       ce qui signifie que vous pourrez toujours y accéder sous son ancien nom. */

    trait E {

        public function saySomething() {

        echo 'Je suis le trait E !';
        }
    }

    class MaClasseCinq {

        use E {

        saySomething as protected;
        }
    }

    $o = new MaClasseCinq;
    $o->saySomething(); // Lèvera une erreur fatale car on tente d'accéder à une méthode protégée.



    trait F {

        public function saySomething() {

        echo 'Je suis le trait A !';
        }
    }

    class MaClasseSix {

        use F {

        saySomething as sayWhoYouAre;
        }
    }

    $o = new MaClasseSix;
    $o->sayWhoYouAre(); // Affichera « Je suis le trait E ! »
    $o->saySomething(); // Affichera « Je suis le trait E ! »



    trait G {

        public function saySomething() {

            echo 'Je suis le trait A !';
        }
    }

    class MaClasseSept {

        use G {

            saySomething as protected sayWhoYouAre;
        }
    }

    $o = new MaClasseSept;
    $o->saySomething(); // Affichera « Je suis le trait G ! ».
    $o->sayWhoYouAre(); // Lèvera une erreur fatale, car l'alias créé est une méthode protégée.



    // Méthodes abstraites dans les traits //

    // On peut forcer la classe utilisant le trait à implémenter certaines méthodes au moyen de méthodes abstraites. Ainsi, ce code lèvera une erreur fatale :


    trait H {

        abstract public function saySomething();
    }

    class MaClasseHuit {

        use H;
    }

    /* Cependant, si la classe utilisant le trait déclarant une méthode abstraite est elle aussi abstraite, alors ce sera à ses classes filles d'implémenter
       les méthodes abstraites du trait (elle peut le faire, mais elle n'est pas obligée). */

       trait I
       {
         abstract public function saySomething();
       }
       
       abstract class mereNeuf
       {
         use I;
       }
       
       // Jusque-là, aucune erreur n'est levée.
       
       class filleNeuf extends mereNeuf
       {
         // Par contre, une erreur fatale est ici levée, car la méthode saySomething() n'a pas été implémentée.
       }

?>
